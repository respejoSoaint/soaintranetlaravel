<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ListController extends Controller
{
    public function getPais(){
        $data = DB::select('select id_pais, descripcion from l_pais order by descripcion asc');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getDescripcionTrabajo(){
        $data = DB::select('select id_descripcion_trabajo, descripcion from l_descripcion_trabajo order by descripcion asc');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getGenero(){
        $data = DB::select('select id_genero, descripcion from l_genero');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getRoles(){
        $data = DB::select('select id_rol, descripcion from l_roles order by descripcion asc');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getProfesion(){
        $data = DB::select('select * from l_profesion');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getCargos(){
        $data = DB::select('select * from l_cargos');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getEstadoCivil(){
        $data = DB::select('select * from l_cargos');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getIdiomas(){
        $data = DB::select('select * from l_idiomas');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getModulos(){
        $data = DB::select('select * from l_modulos');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getNivel(){
        $data = DB::select('select * from l_nivel');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getStatus(){
        $data = DB::select('select * from l_status');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getNacionalidad(){
        $data = DB::select('select * from l_nacionalidad');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getDeducciones(){
        $data = DB::select('select * from nom_l_deducciones');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getAsignaciones(){
        $data = DB::select('select * from nom_l_asignaciones');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getTipoEmpleado(){
        $data = DB::select('select * from nom_l_tipo_empleado');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getAreaTrabajo(){
        $data = DB::select('select * from l_area_trabajo');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getConocimientos(){
        $data = DB::select('select * from l_conocimientos');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getEdoCivil(){
        $data = DB::select('select * from l_edo_civil');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getTipoEstudio(){
        $data = DB::select('select * from l_tipo_estudio');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }

    public function getTipoRequerimiento(){
        $data = DB::select('select * from l_tipo_requerimiento');
        if(count($data)>0){
            $result = array(
                'status' => 'success',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 'error',
                'data' => 'no data'
            );
        }

        return response()->json($result);
    }
}
